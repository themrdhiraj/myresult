<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AdminLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    public function showLoginForm()
    {
        return view('admin.admin-login');
    }

    public function login(Request $request)
    {
        // Validate form data
        $this->validate($request,[
            'email'     =>'required|email',
            'password'  =>'required|min:6'
        ]);
        // Attempt to login
            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // If successful, then redirect to their intended location
            return redirect()->intended(route('admin.dashboard'));
            }
            // If unsuccesfull, then redirect to login with form data
            return redirect()->back()->withInput($request->only('email', 'remember'));
        
    }
}
